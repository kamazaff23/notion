export type NoteType = {
  id: number;
  text: string;
  title: string;
  isPin: boolean;
};
