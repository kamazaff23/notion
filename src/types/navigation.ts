import { ROUTES } from 'names';

export type RootStackParamsList = {
  [ROUTES.MAIN_ROUTES.LIST_NOTES]: undefined;
  [ROUTES.MAIN_ROUTES.NOTE_FORM]: undefined | { id: number };
  [ROUTES.MAIN_ROUTES.NOTE_INFO]: { id: number };
};
