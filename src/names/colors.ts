// Names via https://www.color-name.com/hex

export const BLACK = '#000000';
export const CHINESE_BLACK = '#151517';
export const SONIC_SILVER = '#ADADAD';
export const SILVER_CHALICE = '#F7F6F7';
export const DENIM_BLUE = '#1B41B6';
export const RED = '#FB0007';
