import * as ROUTES from './routes';
import * as COLORS from './colors';

export { ROUTES, COLORS };
