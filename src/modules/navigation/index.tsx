import React, { memo } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { ROUTES } from 'names';
import ListNotes from 'screens/ListNotes';
import NoteForm from 'screens/NoteForm';
import NoteInfo from 'screens/ NoteInfo';
import { RootStackParamsList } from 'types/navigation';

const RootStack = createNativeStackNavigator<RootStackParamsList>();

const Navigator = () => (
  <NavigationContainer>
    <RootStack.Navigator
      initialRouteName={ROUTES.MAIN_ROUTES.LIST_NOTES}
      screenOptions={{ headerShown: false }}
    >
      <RootStack.Screen name={ROUTES.MAIN_ROUTES.LIST_NOTES} component={ListNotes} />
      <RootStack.Screen name={ROUTES.MAIN_ROUTES.NOTE_FORM} component={NoteForm} />
      <RootStack.Screen name={ROUTES.MAIN_ROUTES.NOTE_INFO} component={NoteInfo} />
    </RootStack.Navigator>
  </NavigationContainer>
);

export default memo(Navigator);
