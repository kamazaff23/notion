import React, { memo } from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

import { COLORS } from 'names';

type ButtonPrimaryProps = {
  title: string;
  onPress: () => void;
  isDisabled?: boolean;
};

const defaultProps = {
  isDisabled: false,
};

const ButtonPrimary = ({ title, onPress, isDisabled }: ButtonPrimaryProps) => (
  <TouchableOpacity
    style={[styles.container, isDisabled && styles.backgroundGrey]}
    onPress={onPress}
    disabled={isDisabled}
  >
    <Text style={styles.title}>{title}</Text>
  </TouchableOpacity>
);

ButtonPrimary.defaultProps = defaultProps;

export default memo(ButtonPrimary);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.DENIM_BLUE,
    borderRadius: 10,
  },
  backgroundGrey: {
    backgroundColor: COLORS.SONIC_SILVER,
  },
  title: {
    color: COLORS.SILVER_CHALICE,
    fontSize: 20,
  },
});
