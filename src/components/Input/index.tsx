import React, { memo } from 'react';
import { StyleSheet, TextInput, View, Text, ViewStyle, TextInputProps } from 'react-native';

import { COLORS } from 'names';

type InputProps = {
  value: string;
  onChangeText: (text: string) => void;
  label: string;
  isError?: boolean;
  inputStyle?: ViewStyle | Array<ViewStyle | undefined> | undefined;
} & TextInputProps &
  typeof defaultProps;

const defaultProps = {
  inputStyle: {},
  isError: false,
};

const Input = ({ value, onChangeText, label, inputStyle, isError, ...props }: InputProps) => (
  <View>
    <Text style={styles.label}>{label}</Text>
    <TextInput
      style={[styles.input, inputStyle, isError && styles.borderRed]}
      value={value}
      onChangeText={onChangeText}
      {...props}
    />
  </View>
);

Input.defaultProps = defaultProps;

export default memo(Input);

const styles = StyleSheet.create({
  input: {
    backgroundColor: COLORS.CHINESE_BLACK,
    borderColor: COLORS.DENIM_BLUE,
    borderRadius: 5,
    borderWidth: 1,
    height: 50,
    fontSize: 25,
    paddingHorizontal: 10,
    color: COLORS.SONIC_SILVER,
  },
  label: {
    color: COLORS.SILVER_CHALICE,
    fontSize: 15,
    fontWeight: '500',
    paddingHorizontal: 15,
    paddingVertical: 5,
  },
  borderRed: {
    borderColor: COLORS.RED,
  },
});
