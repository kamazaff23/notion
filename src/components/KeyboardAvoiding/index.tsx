import React, { memo, ReactNode } from 'react';
import { KeyboardAvoidingView, StyleSheet, Platform } from 'react-native';

type KeyboardAvoidingProps = {
  children: ReactNode;
};

const KeyboardAvoiding = ({ children }: KeyboardAvoidingProps) => (
  <KeyboardAvoidingView
    style={styles.container}
    behavior={Platform.OS === 'ios' ? 'padding' : undefined}
  >
    {children}
  </KeyboardAvoidingView>
);

export default memo(KeyboardAvoiding);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
