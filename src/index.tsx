import React, { memo } from 'react';
import { StyleSheet } from 'react-native';
import { Provider } from 'react-redux';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { PersistGate } from 'redux-persist/integration/react';

import { store, persistor } from 'store';
import Navigator from 'modules/navigation';

const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <GestureHandlerRootView style={styles.container}>
        <Navigator />
      </GestureHandlerRootView>
    </PersistGate>
  </Provider>
);

export default memo(App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
