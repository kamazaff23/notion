import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

import { NoteType } from 'types/note';

export interface NotesState {
  notes: Array<NoteType>;
}

const initialState: NotesState = {
  notes: [],
};

export const notesSlice = createSlice({
  name: 'notes',
  initialState,
  reducers: {
    remove: (state, action: PayloadAction<number>) => {
      state.notes = state.notes.filter((note) => note.id !== action.payload);
    },
    create: (state, action: PayloadAction<NoteType>) => {
      state.notes.push(action.payload);
    },
    edit: (state, action) => {
      state.notes = state.notes.map((note) => {
        if (note.id !== action.payload.id) {
          return note;
        }

        return { ...note, ...action.payload };
      });
    },
  },
});

export const { remove, create, edit } = notesSlice.actions;

export default notesSlice.reducer;
