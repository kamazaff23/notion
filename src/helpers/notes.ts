import { NoteType } from 'types/note';

type AccumulatorType = {
  pined: Array<NoteType>;
  unPined: Array<NoteType>;
};

export const splitNotes = (notes: Array<NoteType>) => {
  const processedNodes = notes.reduce(
    (accumulator: AccumulatorType, note) => {
      if (note.isPin) {
        return { ...accumulator, pined: [...accumulator.pined, note] };
      }

      return { ...accumulator, unPined: [...accumulator.unPined, note] };
    },
    {
      pined: [],
      unPined: [],
    },
  );

  return [processedNodes.pined, processedNodes.unPined];
};
