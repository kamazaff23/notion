import React, { memo, useCallback, useMemo } from 'react';
import { Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useSelector } from 'react-redux';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

import { COLORS, ROUTES } from 'names';
import { RootStackParamsList } from 'types/navigation';
import { NoteType } from 'types/note';
import { selectNotes } from 'store/notes/selectors';
import back from 'assets/images/back.png';
import pen from 'assets/images/pen.png';

type NoteInfoProps = NativeStackScreenProps<RootStackParamsList, ROUTES.MAIN_ROUTES.NOTE_INFO>;

const NoteInfo = ({ navigation, route }: NoteInfoProps) => {
  const { id } = route.params;

  const notes = useSelector(selectNotes);

  const selectedNotes = useMemo(
    () => notes.find((note) => note.id === id),
    [id, notes],
  ) as NoteType;

  const handleBackPress = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const handleEditPress = useCallback(() => {
    navigation.navigate(ROUTES.MAIN_ROUTES.NOTE_FORM, { id });
  }, [id, navigation]);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.contentContainer}>
        <View style={styles.headerContainer}>
          <TouchableOpacity
            style={[styles.backContainer, styles.headerSection]}
            onPress={handleBackPress}
          >
            <Image source={back} style={styles.headerImage} />
          </TouchableOpacity>
          <Text style={styles.headerTitle}>Note info</Text>
          <TouchableOpacity
            style={[styles.penContainer, styles.headerSection]}
            onPress={handleEditPress}
          >
            <Image source={pen} style={styles.headerImage} />
          </TouchableOpacity>
        </View>
        <View style={styles.mt30}>
          <Text style={styles.title}>{selectedNotes.title}</Text>
          <Text style={[styles.text, styles.mt20]}>{selectedNotes.text}</Text>
        </View>
      </View>
    </SafeAreaView>
  );
};
export default memo(NoteInfo);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BLACK,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerTitle: {
    color: COLORS.SILVER_CHALICE,
    fontSize: 35,
    fontWeight: '600',
  },
  contentContainer: {
    paddingHorizontal: 15,
    flex: 1,
  },
  headerImage: {
    width: 44,
    height: 44,
    resizeMode: 'contain',
  },
  backContainer: {
    justifyContent: 'center',
  },
  headerSection: {
    flex: 1,
  },
  penContainer: {
    alignItems: 'flex-end',
  },
  title: {
    color: COLORS.SILVER_CHALICE,
    fontSize: 30,
    fontWeight: '600',
  },
  text: {
    color: COLORS.SILVER_CHALICE,
    fontSize: 25,
    fontWeight: '500',
  },
  mt30: {
    marginTop: 30,
  },
  mt20: {
    marginTop: 20,
  },
});
