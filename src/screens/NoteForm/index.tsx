import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import { Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { useDispatch, useSelector } from 'react-redux';

import { COLORS, ROUTES } from 'names';
import { ButtonPrimary, Input, KeyboardAvoiding } from 'components';
import { RootStackParamsList } from 'types/navigation';
import { getUniqKey } from 'services/uniq';
import { NoteType } from 'types/note';
import { create, edit } from 'store/notes/slice';
import { selectNotes } from 'store/notes/selectors';
import back from 'assets/images/back.png';

type NoteFormProps = NativeStackScreenProps<RootStackParamsList, ROUTES.MAIN_ROUTES.NOTE_FORM>;

const NoteForm = ({ navigation, route }: NoteFormProps) => {
  const id = route?.params?.id;

  const dispatch = useDispatch();

  const notes = useSelector(selectNotes);

  const [title, setTitle] = useState('');
  const [text, setText] = useState('');
  const [showError, setShowError] = useState(false);

  const isEditMode = useMemo(() => !!id, [id]);
  const isTitleInputError = useMemo(() => showError && !title, [showError, title]);
  const isTextInputError = useMemo(() => showError && !text, [showError, text]);
  const isButtonDisabled = useMemo(
    () => isTitleInputError || isTextInputError,
    [isTextInputError, isTitleInputError],
  );

  useEffect(() => {
    if (isEditMode) {
      const selectNote = notes.find((note) => note.id === id) as NoteType;

      setText(selectNote.text);
      setTitle(selectNote.title);
    }
  }, []);

  const handleBackPress = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const handleSubmitPress = useCallback(() => {
    if (!title || !text) {
      return setShowError(true);
    }

    if (isEditMode) {
      dispatch(edit({ id, text, title }));

      return navigation.goBack();
    }

    dispatch(create({ text, title, isPin: false, id: getUniqKey() }));
    navigation.goBack();
  }, [id, isEditMode, navigation, text, title]);

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoiding>
        <View style={styles.contentContainer}>
          <View style={styles.headerContainer}>
            <TouchableOpacity
              style={[styles.backContainer, styles.headerSection]}
              onPress={handleBackPress}
            >
              <Image source={back} style={styles.back} />
            </TouchableOpacity>
            <Text style={styles.title}>{isEditMode ? 'Edit node' : 'Create note'}</Text>
            <View style={styles.headerSection} />
          </View>
          <View style={styles.formContainer}>
            <View>
              <Input
                value={title}
                onChangeText={setTitle}
                label="Title"
                isError={isTitleInputError}
              />
              <View style={styles.mt10}>
                <Input
                  value={text}
                  onChangeText={setText}
                  label="Text"
                  inputStyle={styles.textInputStyle}
                  multiline
                  textAlignVertical="top"
                  isError={isTextInputError}
                />
              </View>
            </View>
            <ButtonPrimary
              title={isEditMode ? 'Edit' : 'Create'}
              onPress={handleSubmitPress}
              isDisabled={isButtonDisabled}
            />
          </View>
        </View>
      </KeyboardAvoiding>
    </SafeAreaView>
  );
};

export default memo(NoteForm);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BLACK,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: COLORS.SILVER_CHALICE,
    fontSize: 35,
    fontWeight: '600',
  },
  contentContainer: {
    paddingHorizontal: 15,
    flex: 1,
  },
  textInputStyle: {
    height: 200,
  },
  formContainer: {
    marginTop: 20,
    justifyContent: 'space-between',
    flex: 1,
  },
  back: {
    width: 44,
    height: 44,
    resizeMode: 'contain',
  },
  backContainer: {
    justifyContent: 'center',
  },
  headerSection: {
    flex: 1,
  },
  mt10: {
    marginTop: 10,
  },
});
