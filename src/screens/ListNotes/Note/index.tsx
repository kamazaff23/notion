import React, { memo, useRef } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import { BaseButton } from 'react-native-gesture-handler';
import Swipeable from 'react-native-gesture-handler/Swipeable';

import { COLORS } from 'names';
import pin from 'assets/images/pin.png';
import trash from 'assets/images/trash.png';
import redPin from 'assets/images/redPin.png';

type NoteProps = {
  text: string;
  title: string;
  isPin: boolean;
  onPress: () => void;
  onRemovePress: () => void;
  onPinPress: () => void;
};

const Note = ({ text, title, isPin, onPress, onRemovePress, onPinPress }: NoteProps) => {
  const swipeableRef = useRef<Swipeable>(null);

  const handlePinPress = () => {
    onPinPress();
    setTimeout(() => {
      swipeableRef?.current?.close();
    }, 300);
  };

  const renderRemoveButton = () => (
    <TouchableOpacity style={styles.removeButton} onPress={onRemovePress}>
      <Image source={trash} style={styles.imageButton} />
    </TouchableOpacity>
  );

  const renderPinButton = () => (
    <TouchableOpacity style={styles.pinButton} onPress={handlePinPress}>
      <Image source={redPin} style={styles.imageButton} />
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <Swipeable
        renderLeftActions={renderPinButton}
        renderRightActions={renderRemoveButton}
        ref={swipeableRef}
      >
        <BaseButton onPress={onPress} style={styles.button}>
          {isPin ? <Image source={pin} style={styles.image} /> : null}
          <View>
            <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
              {title}
            </Text>
            <Text style={[styles.text, styles.mt4]} numberOfLines={1} ellipsizeMode="tail">
              {text}
            </Text>
          </View>
        </BaseButton>
      </Swipeable>
    </View>
  );
};

export default memo(Note);

const styles = StyleSheet.create({
  button: {
    backgroundColor: COLORS.CHINESE_BLACK,
    paddingVertical: 10,
    paddingLeft: 30,
    paddingRight: 10,
  },
  container: {
    backgroundColor: COLORS.CHINESE_BLACK,
    overflow: 'hidden',
    borderRadius: 10,
  },
  title: {
    color: COLORS.SONIC_SILVER,
    fontSize: 18,
    fontWeight: '500',
  },
  text: {
    color: COLORS.SONIC_SILVER,
    fontSize: 14,
    fontWeight: '400',
  },
  image: {
    width: 14,
    height: 14,
    marginRight: 10,
    resizeMode: 'contain',
    position: 'absolute',
    top: 12,
    left: 12,
  },
  mt4: {
    marginTop: 4,
  },
  removeButton: {
    width: 80,
    backgroundColor: COLORS.RED,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pinButton: {
    width: 80,
    backgroundColor: COLORS.DENIM_BLUE,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageButton: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
});
