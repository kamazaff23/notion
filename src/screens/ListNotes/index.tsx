import React, { memo, useCallback, useMemo } from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import Animated, { FadeIn, FadeOut, Layout } from 'react-native-reanimated';
import { useDispatch, useSelector } from 'react-redux';

import { COLORS, ROUTES } from 'names';
import { NoteType } from 'types/note';
import { RootStackParamsList } from 'types/navigation';
import { splitNotes } from 'helpers/notes';
import { edit, remove } from 'store/notes/slice';
import { selectNotes } from 'store/notes/selectors';
import Note from 'screens/ListNotes/Note';
import plus from 'assets/images/plus.png';

type ListNotesProps = NativeStackScreenProps<RootStackParamsList, ROUTES.MAIN_ROUTES.LIST_NOTES>;

const ListNotes = ({ navigation }: ListNotesProps) => {
  const dispatch = useDispatch();

  const notes = useSelector(selectNotes);

  const [pinedNotes, unPinedNotes] = useMemo(() => splitNotes(notes), [notes]);
  const processedNotes = useMemo(
    () => [...pinedNotes, ...unPinedNotes],
    [pinedNotes, unPinedNotes],
  );

  const handleAddPress = useCallback(() => {
    navigation.navigate(ROUTES.MAIN_ROUTES.NOTE_FORM);
  }, [navigation]);

  const goToNoteInfo = useCallback(
    (id: number) => {
      navigation.navigate(ROUTES.MAIN_ROUTES.NOTE_INFO, { id });
    },
    [navigation],
  );

  const handleRemove = useCallback((id: number) => {
    dispatch(remove(id));
  }, []);

  const handlePin = useCallback((id: number, isPin: boolean) => {
    dispatch(edit({ id, isPin }));
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.contentContainer}>
        <View style={styles.headerContainer}>
          <Text style={styles.title}>Notes</Text>
          <TouchableOpacity onPress={handleAddPress}>
            <Image source={plus} style={styles.image} />
          </TouchableOpacity>
        </View>
        <ScrollView>
          {processedNotes.map((item: NoteType, index: number) => (
            <Animated.View
              key={item.id}
              entering={FadeIn}
              exiting={FadeOut}
              layout={Layout}
              style={index ? styles.mt6 : {}}
            >
              <Note
                text={item.text}
                title={item.title}
                isPin={item.isPin}
                onRemovePress={() => handleRemove(item.id)}
                onPinPress={() => handlePin(item.id, !item.isPin)}
                onPress={() => goToNoteInfo(item.id)}
              />
            </Animated.View>
          ))}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BLACK,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    color: COLORS.SILVER_CHALICE,
    fontSize: 35,
    fontWeight: '600',
  },
  image: {
    width: 45,
    resizeMode: 'contain',
  },
  contentContainer: {
    paddingHorizontal: 15,
    flex: 1,
  },
  mt6: {
    marginTop: 6,
  },
});

export default memo(ListNotes);
