# Getting started
- yarn install
- cd ios && pod install 
- react-native start
- build project in Android studio and x-code

# Could be better
- ts eslint prettier configs
- make components smaller, move some logics in new files
- move text in constants or in json file for i18n
- add svg icons

# Notion
- developed only for devices with large screen resolution
